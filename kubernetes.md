# Kubernetes

* [Goodbye AWS: Rolling your own servers with Kubernetes, Part 1](https://gravitational.com/blog/aws_vs_colocation/)
* [Networking for Bare Metal Kubernetes Cluster](https://gravitational.com/blog/kubernetes_networking_on_bare_metal)
